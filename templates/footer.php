<?php $lang = pll_current_language(); ?>
<footer class="content-info" role="contentinfo">
    <div class="container">
        <div class="row ">
            <div class="col-lg-10 col-lg-push-1 footer">
                <ul>
                    <li> &COPY; <?php echo date('Y') . " YummyBites" ?></li>
                    <li> Website by MIMOSA | graphic lab</li>
                    <li> <a href="#top" class="toTop"><i class="fa fa-arrow-circle-up"></i> take me back to top</a></li>
                </ul>
            </div>
        </div>

    </div>
</footer>
