<?php
$lang = 'en';
if (function_exists('pll_current_language'))
    $lang = pll_current_language();

//$data = get_post_by(array('en' => 'product', 'id' => 'produk'));
$data = get_product_list();
?>
<div id="product" class="products clearfix">
    <div class="container">
        <div id="product" class="col-xs-12">
            <h2 class="text-center title"><?php echo ($lang == 'en') ? 'Product' : 'Produk' ?></h2>
            <?php
            foreach ($data as $key => $value) {
                $class = ($key > 0) ? '' : 'col-lg-offset-1 col-md-offset-1';
                $url = get_permalink($value->ID);
                $img = get_the_post_thumbnail($value->ID, 'full', array('class' => 'img-responsive'));
                $color = get_post_meta($value->ID, 'color', true)
                ?>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 product <?php echo $class . " " . strtolower($color) ?>">
                    <a href="<?php echo $url ?>" title="<?php echo $value->post_title ?>">
                        <?php echo $img ?>
                        <?php echo $value->post_title ?>
                    </a>
                </div>
                <?php
            }
            ?>
        </div>
        <p class="see-all-product text-center">
            <a href="<?php echo ($lang == 'en') ? home_url('products') : home_url($lang . '/products') ?>" class="btn btn-lg">
                <?php echo ($lang == 'en') ? "SEE ALL OUR PRODUCT" : "LIHAT SEMUA PRODUK" ?>
            </a>

        </p>
    </div>
</div>
<div id="where" class="where-to-buy clearfix">
    <div class="container">
        <h2 class="text-center title"><?php echo ($lang == 'en') ? 'WHERE TO BUY' : 'MAU BELI' ?></h2>
        <!--        <h3>RETAIL PARTNER</h3>-->
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <?php dynamic_sidebar('Where To Buy 1') ?>
            </div>
            <div class="col-lg-3 col-md-3">
                <?php dynamic_sidebar('Where To Buy 2') ?>
            </div>
            <div class="col-lg-3 col-md-3">
                <?php dynamic_sidebar('Where To Buy 3') ?>
            </div>
            <div class="col-lg-3 col-md-3">
                <?php dynamic_sidebar('Where To Buy 4') ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <?php dynamic_sidebar('Where To Buy 5') ?>
            </div>
        </div>
    </div>
</div>
<div id="newsevent" class="news clearfix">
    <?php
    $events = get_post_by(array('en' => 'news-event', 'id' => 'berita-event'), 2);
    ?>
    <div class="container">
        <h2 class="text-center title"><?php echo ($lang == 'en') ? 'NEWS & EVENT' : 'BERITA & EVENT' ?></h2>
        <div class="row">
            <?php
            foreach ($events['posts_array'] as $ke => $ve) {
                setup_postdata($ve);
                $cat = get_the_category($ve->ID);
                ?>
                <div class="col-lg-12">
                    <div class="col-lg-6 col-sm-12">
                        <div class="frame col-lg-12">
                            <?php echo get_the_post_thumbnail($ve->ID, 'yummy-thumbnail', array('class' => 'img-responsive news-img')) ?>
                        </div>

                    </div>
                    <div class="col-lg-6 col-sm-12 <?php echo $cat[0]->slug ?>">
                        <h4>
                            <img src="<?php echo get_template_directory_uri() ?>/assets/img/<?php echo $cat[0]->slug ?>.png" alt="<?php echo $ve->post_title ?>"/>
                            <?php
                            echo $ve->post_title
                            ?>
                        </h4>
                        <?php
                        the_content(' ');
                        ?>
                        <a href="<?php echo get_permalink($ve->ID) ?>" class="more-link">
                            READ MORE
                        </a>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<div id="video" class="youtube clearfix">
    <div class="container">
        <h2 class="title"><?php echo ($lang == 'en') ? 'We\'re on tv' : 'Kita di Tv' ?></h2>
        <div class="col-lg-6 col-lg-push-3 video">
            <div class="clearfix">
                <div class="cycle-slideshow" 
                     data-cycle-pager="#custom-pager"
                     data-cycle-pager-template="<strong><a href=#> {{slideNum}} </a></strong>"
                     data-cycle-fx="scrollHorz" 
                     data-cycle-timeout="0"
                     data-cycle-slides="> iframe"
                     >
                         <?php echo do_shortcode('[video_list]') ?>
                </div>
            </div>

            <!--            <div class="col-lg-10 col-lg-push-1 text-center">
                            <div class="iframe">
                                <div class="cycle-slideshow" 
                                     data-cycle-pager="#custom-pager"
                                     data-cycle-pager-template="<strong><a href=#> {{slideNum}} </a></strong>"
                                     data-cycle-fx="scrollHorz" 
                                     data-cycle-timeout="0"
                                     data-cycle-slides="> iframe"
                                     >
            <?php // do_shortcode('[video_list]') ?>
                                </div>
                            </div>
                        </div>-->
        </div>
        <div class="clear"></div>
        <div id="custom-pager" class="col-lg-12 text-center"></div>
    </div>
</div>
<div id="contact" class="contact clearfix">
    <div class="container">
        <h2 class="title text-center"><?php echo ($lang == 'en') ? 'Contact' : 'Kontak' ?></h2>
        <?php
        $contacForm = do_shortcode("[get_form]");
        echo do_shortcode(stripslashes($contacForm));
        ?>
        <div class="row other">
            <div class="col-lg-6 col-lg-push-3">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
                    <i class="fa fa-mobile fa-4x"></i><br/>
                    <?php dynamic_sidebar('contact') ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
                    <i class="fa fa-envelope-o fa-4x"></i><br/>
                    <?php dynamic_sidebar('email') ?>
                </div>
            </div>
        </div>

    </div>
</div>