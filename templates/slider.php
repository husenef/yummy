<?php
$sliders = all_options('slider');
?>
<section class="clearfix slider">
    <div class="container">
        <div class="spinner">
            <div class="spinner-loader">
                Loading…
            </div>
        </div>
        <div class="row frame" style="position: relative">
            <div class="col-lg-10 col-lg-push-1 col-md-12 col-md-push-0 col-xs-push-2 col-xs-8 ">
                <ul id="roundabout">
                    <?php
                    $s = 0;
                    for ($e = 1; $e <= 2; $e++) {
                        foreach ($sliders as $slide) {
                            $status = (isset($slide->status)) ? 'new' : '';
                            echo "<li class='y-$s $status'><img src='$slide->img' class='img-responsive'/></li>";
                            $s++;
                        }
                    }
                    ?>
                </ul>
                <!--            <div class="button row">
                                <span class="prev fa fa-2x fa-chevron-left"></span>
                                <span class="next fa fa-2x fa-chevron-right pull-right"></span>
                            </div>-->
            </div>
            <div class="button">
                <span class="prev fa fa-2x fa-chevron-left"></span>
                <span class="next fa fa-2x fa-chevron-right pull-right"></span>
            </div>
        </div>
    </div>
</section>