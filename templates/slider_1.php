<?php
$lang = 'en';
if (function_exists('pll_current_language'))
    $lang = pll_current_language();
//$post_cat = get_posts(array('category_name' => ($lang == 'en') ? 'product' : 'produk', 'order' => 'ASC'));
$post_cat = get_product_list(array('order' => 'ASC', 'numberposts' => '-1'));
//$post_cat = array(
//    'product1.png',
//    'product2.png',
//    'product3.png',
//    'product4.png',
//    'product5.png',
//    'product3.png',
//    'product4.png',
//    'product5.png'
//);
?>
<section class="clearfix slider">
    <div class="container">

        <div class="spinner">
            <div class="spinner-loader">
                Loading…
            </div>
        </div>

        <div class="row frame" style="position: relative">

            <div class="col-lg-10 col-lg-push-1 col-md-12 col-md-push-0 col-xs-push-2 col-xs-8 ">
                <ul id="roundabout">
                    <?php
                    $s = 0;
                    for ($e = 1; $e <= 2; $e++) {
                        foreach ($post_cat as $value) {
                            $status = get_post_meta($value->ID, 'status', true);
                            echo "<li class='y-$s $status'>" . get_the_post_thumbnail($value->ID, 'full', array('class' => 'img-responsive')) . "</li>";
                            $s++;
                        }
                    }
                    wp_reset_query();
                    ?>
                </ul>
                <!--            <div class="button row">
                                <span class="prev fa fa-2x fa-chevron-left"></span>
                                <span class="next fa fa-2x fa-chevron-right pull-right"></span>
                            </div>-->
            </div>
            <div class="button">
                <span class="prev fa fa-2x fa-chevron-left"></span>
                <span class="next fa fa-2x fa-chevron-right pull-right"></span>
            </div>
        </div>
    </div>
</section>