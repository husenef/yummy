<?php while (have_posts()) : the_post(); ?>
    <article <?php post_class(); ?>>
        <?php
//        echo do_shortcode("[carousel post_type='attachment' thickbox='0']");
        if (shortcode_exists("[carousel]")):
            echo do_shortcode("[featured-video-plus]");
        elseif (shortcode_exists("[featured-video-plus]")) :
            echo do_shortcode("[carousel post_type='attachment' thickbox='0']");
        else:
            the_post_thumbnail('full', array('class' => 'img-responsive'));
        endif;
//        the_post_thumbnail('full', array('class' => 'img-responsive'));
        ?>
        <header>
            <?php
            $cat = get_the_category(get_the_ID());
            ?>
            <h1 class="entry-title">
                <?php
                if (file_exists(get_template_directory() . "/assets/img/" . $cat[0]->slug . '.png')):
                    echo "<img src='" . get_template_directory_uri() . "/assets/img/" . $cat[0]->slug . ".png' class=''/>";
                endif;
                ?>
                <?php the_title(); ?>
            </h1>
            <?php //get_template_part('templates/entry-meta'); ?>
        </header>
        <div class="entry-content">
            <?php the_content(); ?>
        </div>
        <footer>
            <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'roots'), 'after' => '</p></nav>')); ?>
        </footer>
        <?php // comments_template('/templates/comments.php'); ?>
    </article>
<?php endwhile; ?>
