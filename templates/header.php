<header class="banner navbar " role="banner">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="social hidden-lg visible-xs-inline visible-sm-inline visible-md-inline text-center pull-right">
                <?php echo do_shortcode("[social]") ?>
                <ul class="language-chooser">
                    <?php pll_the_languages(array('dropdown' => 0, 'show_flags' => 1, 'show_names' => 0)) ?>
                </ul>
            </div>
            <a class="" href="<?php echo esc_url(home_url('/')); ?>">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo.png" alt="<?php bloginfo() ?>" class="img-responsive"/>
            </a>
        </div>
        <?php /* <div class="social hidden-lg hidden-md hidden-sm hidden-xs text-center pull-right">
          <?php echo do_shortcode("[social]") ?>
          <ul class="language-chooser">
          <?php pll_the_languages(array('dropdown' => 0, 'show_flags' => 1, 'show_names' => 0)) ?>
          </ul>
          </div> */ ?>
        <nav class="collapse navbar-collapse" role="navigation">
            <div class="social hidden-md hidden-sm hidden-xs">
                <?php echo do_shortcode("[social]") ?> 
                <ul class="language-chooser">
                    <?php pll_the_languages(array('dropdown' => 0, 'show_flags' => 1, 'show_names' => 0)) ?>
                </ul>
            </div>
            <?php
            if (has_nav_menu('primary_navigation') && is_front_page()) :
                wp_nav_menu(array('theme_location' => 'primary_navigation', 'walker' => new Roots_Nav_Walker(), 'menu_class' => 'nav navbar-nav'));
            else:
                wp_nav_menu(array('theme_location' => 'second_navigation', 'walker' => new Roots_Nav_Walker(), 'menu_class' => 'nav navbar-nav'));
            endif;
            ?>
        </nav>
    </div>
</header>
