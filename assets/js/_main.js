/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can 
 * always reference jQuery with $, even when in .noConflict() mode.
 *
 * Google CDN, Latest jQuery
 * To use the default WordPress version of jQuery, go to lib/config.php and
 * remove or comment out: add_theme_support('jquery-cdn');
 * ======================================================================== */

(function ($) {

// Use this variable to set up the common and page specific functions. If you 
// rename this variable, you will also need to rename the namespace below.
    var Roots = {
        // All pages
        common: {
            init: function () {
                // JavaScript to be fired on all pages
                $('a.toTop').on('click', function (e) {
                    e.preventDefault();
                    $(window).scrollTo(0, 2000, {queue: true});
                    return false;
                });

                $('ul.nav a').on('click', function (e) {
                    var winWid = $(window).width();
                    var target = $(this).attr('href');
                    var minTop = 108;
                    if (target.indexOf('#') !== -1) {
//                        var pos = $(target).position();
//                        e.preventDefault();
//                        console.log({
//                            top: pos.top,
//                            id: target
//                        });
//                      
                        if (winWid <= 555) {
                            minTop = 150;
                        }
                        $(window).scrollTo(target, 2000, {offset: {top: -minTop}});
//                        $(window).scrollTo((pos.top - minTop), 2000, {queue: true});
                        return false;
                    }
                });
//                if ($('#roundabout').size()) {
//                    $('#roundabout').roundabout({
//                        btnNext: ".next",
//                        btnPrev: ".prev",
////                        shape: "square",
//                        minOpacity: 1.0,
//                        responsive: true,
//                        minScale: 0.5,
//                        autoplay: true,
//                        autoplayDuration: 3000,
//                        autoplayPauseOnHover: true
//                    });
//                }

                $('.banner').sticky({topSpacing: 0});
            }
        },
        // Home page
        home: {
            init: function () {
                // JavaScript to be fired on the home page

            }
        },
        // About us page, note the change from about-us to about_us.
        about_us: {
            init: function () {
                // JavaScript to be fired on the about us page
            }
        }
    };

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function (func, funcname, args) {
            var namespace = Roots;
            funcname = (funcname === undefined) ? 'init' : funcname;
            if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function () {
            UTIL.fire('common');

            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
                UTIL.fire(classnm);
            });
        }
    };

    $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
$(window).load(function () {
    if ($('#roundabout').size()) {
        $('#roundabout').roundabout({
            btnNext: ".next",
            btnPrev: ".prev",
//                        shape: "square",
            minOpacity: 1.0,
            responsive: true,
            minScale: 0.5,
            autoplay: true,
            autoplayDuration: 3000,
            autoplayPauseOnHover: true
        });
    }
});
$(window).bind("load", function () {
    $('.spinner').animate({opacity: 0}, 500, function () {
        setInterval(500, $('.frame').animate({opacity: 1}));
    });
    var winWid = $(window).width();
    var minTop = 108;
    var target = $(location).attr('hash');

    if (winWid <= 555) {
        minTop = 150;
    }
    if (target !== '') {
        $(window).scrollTo(target, 2000, {offset: {top: -minTop}});
    }
//    $('.spinner-loader').fadeOut('slow', function () {
////        setInterval(2000, $('.frame').show());
//    });
//    $('.slider').fadeIn();
});