/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
jQuery(document).ready(function ($) {
    $('.delet').on('click', function () {
        var answer = confirm('Apakah video akan di hapus?');
        var id = $(this).data('id');
        if (answer) {
            $('.video' + id).remove();
        } else {
            return false;
        }
    });
    $('.add-more').on('click', function () {
        var html = "<div class='appendyoutube'>" +
                "<input type='text' name='youtube[]' placeholder='id Youtube' class='' /></div>";
        $(html).appendTo('.append');
    });

    $('.remove').on('click', function () {
        $('.append .appendyoutube').last().remove();
    });
    /*slider*/

    var num = $('.frame-slide').find('.admin-slide').size();

    $('.add-slider').on('click', function () {
//        alert(num);
        var slider = "<div>" +
                "<input type='text' name='slider[" + num + "][img]' placeholder='URL Slider' class='link regular-text'/>" +
                "&nbsp;<button type='button' class='upload' onclick='javascript:uploadJs(this)'>Upload</button>" +
                "&nbsp;<input type='checkbox' name='slider[" + num + "][status]' tile=''/> Produk Baru " +
                "</div>";
        $(slider).appendTo('.append-slider');
        num++;
    });
    $('.remove-slider').on('click', function () {
        $('.append-slider div').last().remove();
        num--;
    });
    $('.remove-slide').on('click', function () {
        var answer = confirm('Apakah Slide ini akan di hapus?');
        var id = $(this).data('id');
        if (answer) {
            $('.slide-' + id).fadeOut('slow', function () {
                $(this).remove();
            });
        } else {
            return false;
        }
    });



});

function uploadJs(a) {
    jQuery(function ($) {
        var custom_uploader;

//        $('.upload').on('click', function (e) {
//            e.preventDefault();

        var button = $(a);

        // if (custom_uploader) {
        //           custom_uploader.open();
        //           return;
        //       }

        //Extend the wp.media object
        custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
            multiple: false
        });

        //When a file is selected, grab the URL and set it as the text field's value
        custom_uploader.on('select', function () {
            attachment = custom_uploader.state().get('selection').first().toJSON();
            button.siblings('.link').val(attachment.url);
        });

        //Open the uploader dialog
        custom_uploader.open();
    });
//    });
}