<?php
/*
  Template Name: Home Template
 */
?>
<div id="aboutus" class="before-home clearfix">
    <div class="col-lg-6">
        <div class="clearfix block-1">
            <div class="col-lg-12">
                <?php dynamic_sidebar('Block Home 1') ?>
            </div>
        </div>
        <div class="clearfix block-2">
            <?php dynamic_sidebar('Block Home 2') ?>
<!--            <img src="<?php echo get_template_directory_uri() ?>/assets/img/baby.png" class="img-responsive"/>-->
        </div>
    </div>
    <div class="col-lg-6 block-3">
        <?php dynamic_sidebar('Block Home 3') ?>
<!--        <img src="<?php echo get_template_directory_uri() ?>/assets/img/statistik-green.png" class="img-responsive"/>-->
    </div>
</div>
<div class="col-lg-12">
    <div class="welcome-page clearfix">
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <?php while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; ?>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-lg hidden-md hidden-sm visible-xs-block">
            <div class="row">
                <img src="<?php echo get_template_directory_uri() ?>/assets/img/baby-bg.png" class="img-responsive"/>
            </div>
        </div>
    </div>
</div>