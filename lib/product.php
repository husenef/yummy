<?php

//register product
function product() {
//    pll_get_post_types();
    $args = array(
        'label' => __('Product', 'yummy'),
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'custom-fields'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'rewrite' => array('slug' => 'products'),
//        'taxonomies' => array('category')
    );
    register_post_type('product', $args);
}

add_action('init', 'product');

$taxonomi = array(
    'hierarchical' => true,
    'labels' => array('name' => 'Product Category'),
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'product'),
);

register_taxonomy('product-category', array('product'), $taxonomi);
//register lang to product
add_filter('pll_get_post_types', 'my_pll_get_post_types');

function my_pll_get_post_types($types) {
    return array_merge($types, array('product' => 'product'));
}

function custom_breadcrumb() {
    if (!is_home()) {
        echo '<ol class="breadcrumb">';
        echo '<li><a href="' . get_option('home') . '">Home</a></li>';
        if (is_single()) {
            if (!is_category()) {
                if (is_single('product')) {
                    echo'<li>Products</li>';
                    echo '<li>';
                    the_title();
                    echo '</li>';
                } elseif (is_single()) {
                    echo '<li>';
                    the_title();
                    echo '</li>';
                }
            } else {
                echo '<li>';
                the_category(', ');
                echo '</li>';
                if (is_single()) {
                    echo '<li>';
                    the_title();
                    echo '</li>';
                }
            }
        } elseif (is_category()) {
            echo '<li>';
            single_cat_title();
            echo '</li>';
        } elseif (is_page() && (!is_front_page())) {
            echo '<li>';
            the_title();
            echo '</li>';
        } elseif (is_tag()) {
            echo '<li>Tag: ';
            single_tag_title();
            echo '</li>';
        } elseif (is_day()) {
            echo'<li>Archive for ';
            the_time('F jS, Y');
            echo'</li>';
        } elseif (is_month()) {
            echo'<li>Archive for ';
            the_time('F, Y');
            echo'</li>';
        } elseif (is_year()) {
            echo'<li>Archive for ';
            the_time('Y');
            echo'</li>';
        } elseif (is_author()) {
            echo'<li>Author Archives';
            echo'</li>';
        } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
            echo '<li>Blog Archives';
            echo'</li>';
        } elseif (is_search()) {
            echo'<li>Search Results';
            echo'</li>';
        } elseif (is_post_type_archive('product')) {
            echo'<li>Products</li>';
        } elseif (is_post_type_hierarchical('product')) {
            echo'<li>Products</li>';
        }
        echo '</ol>';
    }
}
