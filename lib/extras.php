<?php

/**
 * Clean up the_excerpt()
 */
function roots_excerpt_more() {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'roots') . '</a>';
}

add_filter('excerpt_more', 'roots_excerpt_more');

function get_post_by(array $slug, $limit = 5) {
    $lang = 'en';
    if (function_exists('pll_current_language'))
        $lang = pll_current_language();
    $args = array(
        'posts_per_page' => $limit,
        'offset' => 0,
        'category_name' => ($lang == 'en') ? $slug['en'] : $slug['id'],
        'post_type' => 'post',
        'post_status' => 'publish',
        'suppress_filters' => true
    );
    $category = get_category_by_slug(($lang == 'en') ? $slug['en'] : $slug['id']);
// Get the URL of this category
    $data['category_link'] = get_category_link($category->term_id);
    $data['posts_array'] = get_posts($args);
    wp_reset_query();
    return $data;
}

function register_widgetr_init() {
//register sider home
    $argsHome = array(
        'name' => __('Where To Buy %d'),
        'id' => 'home-sidebar',
        'description' => __('untuk title ke 2-4, isi dengan "&-N-B-S-P-;" tanpa "-"'),
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>');

    register_sidebars(5, $argsHome);
//register sider home
    $bloxkHome = array(
        'name' => __('Block Home %d'),
        'id' => 'block-home-sidebar',
        'description' => '',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>');

    register_sidebars(3, $bloxkHome);
    $contact = array(
        'name' => __('Contact', 'yummi'),
        'id' => 'contact',
        'description' => 'For contact Home',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<span class="title">',
        'after_title' => '</span>');
    register_sidebar($contact);
    $email = array(
        'name' => __('Email', 'yummi'),
        'id' => 'email',
        'description' => 'For contact Home',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<span class="title">',
        'after_title' => '</span>');
    register_sidebar($email);
    $product = array(
        'name' => __('Product', 'yummi'),
        'id' => 'product',
        'description' => 'For product page',
        'class' => '',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<span class="title">',
        'after_title' => '</span>');
    register_sidebar($product);
}

add_action('widgets_init', 'register_widgetr_init');


add_shortcode("get_form", "get_form7");

function get_form7($attr, $content) {
    return all_options('form7');
}

add_shortcode("social", "social_link");

function social_link($attr, $content) {
    $ig = all_options('ig');
    $fb = all_options('facebook');
    $html = "";
    if ($ig)
        $html .= " <a href='http://instagram.com/$ig' target='blank' class='ig'><span>IG</span></a>";
    if ($fb)
        $html .= "<a target='blank' href='http://www.facebook.com/$fb'><span>fb</span></a>";
    return $html;
}

add_shortcode("video_list", "get_video");

function get_video($attr) {
    $html = '';
    $youtube = all_options('youtube');
//    $youtubes = explode(",", $youtube);
    foreach ($youtube as $ytb) {
        if ($ytb != "")
//            echo "<iframe width='auto' height='auto' src='https://www.youtube.com/embed/$ytb' frameborder='0' scrolling='no' allowfullscreen></iframe>";
            $html.= "<iframe width='auto' height='auto' src='https://www.youtube.com/embed/$ytb?rel=0&amp;controls=0&amp;showinfo=0' frameborder='0' scrolling='no' allowfullscreen></iframe>";
    }
    return $html;
}

function all_options($key = null) {
    $opt = get_option('yummyOptions');
    $options = json_decode($opt);
    if ($key):
        return ($options->$key);
    else:
        return $options;
    endif;
}

function get_product_list($array = array()) {
    $filter = array('post_type' => 'product');
    if ($array)
        $filter = array_merge($filter, $array);
    return get_posts($filter);
}
