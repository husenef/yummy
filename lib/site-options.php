<?php

if (!defined('ABSPATH')) {
    exit; // disable direct access
}
if (!class_exists('Site_Options')) :

    class Site_Options {

        var $siteOptions = null;
        var $optionName = 'yummyOptions';
        var $options = array();

        public static function init() {
            $siteOptions = new self();
        }

        function __construct() {
            $this->add_script();
            $this->save_options();
            $this->set_options();
            $this->pages();
        }

        function pages() {
            $html = "";
            $html .= "<h3>Site Options</h3>";
            $html .= "<form action='' method='post'>";
            $html .="<table class='form-table'>";
            $html .="<tbody><tr>";
            $html .="<th scope='row'><label for='metaslider'>Contac Form 7</label></th>";
            $html .="<td><input type='text' name='form7' value='" . stripslashes($this->options->form7) . "' placeholder='Kode contact form 7' /></td>";
            $html .="</tr></tbody></table>";
            $html .="<h3>Social Media</h3><hr/>";
            $html .="<table class='form-table'><tbody><tr>";
            $html .="<th scope='row'><label for='facebook'>Facebook</label></th>";
            $html .="<td><input type='text' name='facebook' value='" . $this->options->facebook . "' id='facebook'/> Ex : <span class='description'>http://www.facebook.com/<strong>Facebook code</strong></span></td></tr>";
            $html .="<tr><th scope='row'><label for='ig'>Instagram</label></th>";
            $html .="<td><input type='text' name='ig' value='" . $this->options->ig . "' id='ig'/> Ex : <span class='description'>https://instagram.com/+<strong>IG Code</strong></span></td>";
            $html .="</tr></tbody></table>";
            /* slider */
            $html .= $this->sliderMain($this->options->slider);
            /* slider */
            $html .="<h3>Youtube</h3><hr/>";
            $html .="<table class='form-table'><tbody><tr>";
            $html .="<th scope='row'><label for='youtube'>Youtbe ID</label></th>";
//            $html .="<td><textarea name='youtube' placeholder='Id Youtube'>".$this->options->youtube ."</textarea> <br/>Ex : <span class='description'>https://www.youtube.com/watch?v=<strong>ID_YOUTUBE</strong></span>, <br/>gunakan koma(,) untuk menampilkan lebih dari 1 video. Ex : KiNRIR8tjFc <strong>,</strong> nPLV7lGbmT4</td></tr>";
            $html .="<td> " . $this->youtube($this->options->youtube) . "</td></tr>";
            $html .="</tbody></table>";
            $html .="<p class='submit'><input type='submit' value='Update Options' class='button button-primary' id='submit'></p> ";
            $html .="</form>";
            echo $html;
        }

        function save_options() {
            if ($_POST):

                $data = json_encode($_POST);

                if (get_option($this->optionName) !== false) {
                    update_option($this->optionName, $data);
                } else {
                    $deprecated = null;
                    $autoload = 'yes';
                    add_option($this->optionName, $data, $deprecated, $autoload);
                }
                $_SESSION['message'] = array('c' => '1', 'm' => 'Data Sudah di simpan');
//                $this->redirect(admin_url('themes.php?page=theme_option.php'));
            endif;
        }

        function set_options() {
            $options = get_option($this->optionName);
            $this->options = json_decode($options);

            return $this;
        }

        function add_script() {
            wp_enqueue_media();
            echo "<link type='text/css' rel='stylesheet' href='" . get_template_directory_uri() . "/assets/css/admin.css'/>";
            echo "<script type='text/javascript' src='" . get_template_directory_uri() . "/assets/js/admin/js.js'></script>";
        }

        function redirect($url) {
            echo "<script type='text/javascript'>window.location = '" . $url . "';</script>";
        }

        function get_form() {
            $form = $this->options->form7;
            return stripslashes($form);
        }

        function youtube($youtube = '') {
            if (empty($youtube))
                return '';

            $vid = $return = '';
            foreach ($youtube as $ky => $id) {
                if ($id != "") {
                    $vid .= "<div class='vide video$ky'><input type='hidden' value='$id' name='youtube[]'/>"
                            . "<iframe width='auto' height='auto' src='https://www.youtube.com/embed/$id' frameborder='0' scrolling='no' allowfullscreen></iframe><br/><button type='button' class='delet  button-primary' data-id='$ky'>delete video</button></div><br/>";
                }
            }
            if ($vid == '')
                $vid = "<input type='text' name='youtube[]' placeholder='id Youtube' class='regular-text'/>&nbsp;";
            $return .= $vid;
            $return .= "<button type='button' class='add-more button-primary'> + Tambah video</button> &nbsp;<button type='button' class='remove button-primary'> - Hapus video</button>";
            $return .="<p class='description'><strong>Contoh:</strong> https://www.youtube.com/watch?v=<strong>2EkaHFFfbhI</strong></p>";
            $return .="<div class='append'></div>";
            return $return;
        }

        function sliderMain($slider) {
            $html .="<h3>Slider Home</h3><hr/>";
            $html .="<table class='form-table'><tbody><tr>";
            $html .="<th scope='row'><label for='slider'>Slider Home</label></th>";
//            $html .="<td><textarea name='youtube' placeholder='Id Youtube'>".$this->options->youtube ."</textarea> <br/>Ex : <span class='description'>https://www.youtube.com/watch?v=<strong>ID_YOUTUBE</strong></span>, <br/>gunakan koma(,) untuk menampilkan lebih dari 1 video. Ex : KiNRIR8tjFc <strong>,</strong> nPLV7lGbmT4</td></tr>";
            $html .="<td> " . $this->sliderForm($slider) . "</td></tr>";
            $html .="</tbody></table>";
            return $html;
        }

        function sliderForm($slider = '') {
            $slide = '';
            $slide .= "<button type='button' class='add-slider button-primary'> + Tambah Slider</button> &nbsp;<button type='button' class='remove-slider button-primary'> - Hapus Slider</button>";
            $slide.= "<p class='description'>untuk Ukuran yang di sarankan adalah <strong> 386 x 453 px</strong><br/>Untuk penampilan gambar, akan <strong>diulang 1x</strong> tiap gambar,jadi di halaman depan akan ada 2 tiap gambar</p>";
            $slide .="<hr/><div class='append-slider'></div>";
            if (!empty($slider)):
                $slide .="<div class='frame-slide'>";
                foreach ($slider as $s):
                    $sliders[] = $s;
                endforeach;
               
                foreach ($sliders as $key => $image) :
                    if ($image->img != ''):

                        $checked = (isset($image->status)) ? "checked='checked'" : '';
                        $slide .="<div class='admin-slide slide-$key' data-num='$key'><input type='hidden' value='$image->img' name='slider[$key][img]'/>";
                        $slide .="&nbsp;<input type='checkbox' name='slider[$key][status]' $checked/> Produk Baru <button type='button' class='remove-slide' data-id='$key'> Hapus</button><br/>";
                        $slide .="<img src='$image->img' style='max-width:200px'/>";
                        $slide.= "</div>";
                    endif;
                endforeach;
                $slide.= "</div>";
            endif;
//            if ($slider == '')
            //$slide .= "<div><input type='text' name='slider[]' placeholder='Url Slider' class='regular-text'/>&nbsp;<button type='button' class='upload'>Upload</button></div>";

            return $slide;
        }

    }

    endif;

function yummy_themeoption() {
    add_theme_page('Site Option', 'YummyBites Site Options', 'manage_options', 'theme_option.php', array('Site_Options', 'init'));
}

add_action('admin_menu', 'yummy_themeoption');
