<div class="row">
    <div class="title-product col-lg-12">
        <h2>PRODUCT</h2>
        <?php
        if (function_exists('custom_breadcrumb')) {
            custom_breadcrumb();
        }
        ?>
    </div>
    <div class="col-lg-4">
        <div class="sidebar-product col-lg-12">
            <i class="fa fa-sort-desc pull-left"></i>
            <?php dynamic_sidebar('product') ?>
        </div>
    </div>
    <div class="col-lg-8 ">
        <div class="product-page col-lg-12">
            <?php
            $tax = (get_the_category());
            echo "<h4 class='taxonomies'>".$tax[0]->name."</h4>";
            ?>

            <div class="row product-list">
                <?php
                $n = 1;
                while (have_posts()) : the_post();
                    ?>
                    <div class="col-lg-3">
                        <a href="<?php the_permalink() ?>">
                            <?php the_post_thumbnail('full', array('class' => 'img-responsive')) ?>
                        </a>
                        <a href="<?php the_permalink() ?>">
                            <?php $class = get_post_meta(get_the_ID(), 'color', true) ?>
                            <?php the_title("<span class='$class'>", "</span>", true) ?>
                        </a>
                    </div>
                    <?php
                    if (($n % 4) == 0)
                        echo "</div><div class='row product-list'>";
                    $n++;
                    ?>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</div>