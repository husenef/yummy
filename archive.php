<div class="row">
    <div class="title-product col-lg-12">
        <h2><?php _e("NEWS & EVENT", 'yummy') ?></h2>
        <?php
        if (function_exists('custom_breadcrumb')) {
            custom_breadcrumb();
        }
        ?>
    </div>
    <div class="col-lg-4">
        <div class="sidebar-product col-lg-12">
            <?php dynamic_sidebar('sidebar-primary'); ?>
        </div>
    </div>
    <div class="col-lg-8 ">
        <div class="news-event-archive col-lg-12">
            <?php
            while (have_posts()) : the_post();
                ?>
                <div class='the-content'>
                    <div>
                        <?php
                        $cat = get_the_category(get_the_ID());
                        if (shortcode_exists("[featured-video-plus]")):
                            echo do_shortcode("[featured-video-plus]");
                        else:
                            the_post_thumbnail('full', array('class' => 'img-responsive'));
                        endif;
                        ?>
                        <h3>
                            <?php
                            if (file_exists(get_template_directory() . "/assets/img/" . $cat[0]->slug . '.png')):
                                echo "<img src='" . get_template_directory_uri() . "/assets/img/" . $cat[0]->slug . ".png' class=''/>";
                            endif;
                            the_title();
                            ?>
                        </h3>
                        <?php
                        the_content('');
                        ?>
                        <a href='<?php echo get_permalink() ?>' class='more-link'>
                            <?php _e("READ MORE") ?>
                        </a> 
                    </div>

                </div>
                <?php
            endwhile;
            ?>
        </div>
    </div>
</div>
</div>