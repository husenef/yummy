<div class="row">
    <div class="title-product col-lg-12">
        <h2><?php _e("NEWS & EVENT", 'yummy') ?></h2>
        <?php
        if (function_exists('custom_breadcrumb')) {
            custom_breadcrumb();
        }
        ?>
    </div>
    <div class="col-lg-4">
        <div class="sidebar-product col-lg-12">
            <?php dynamic_sidebar('sidebar-primary'); ?>
        </div>
    </div>
    <div class="col-lg-8 ">
        <div class="news-event col-lg-12">
            <?php get_template_part('templates/content', 'single'); ?>
            <?php /* while (have_posts()) : the_post(); ?>
              <?php $class = get_post_meta(get_the_ID(), 'color', true) ?>
              <?php the_title("<h3 class='$class'>", '</h3>') ?>
              <div class="frame-slider-product ">
              <?php echo do_shortcode("[photospace_res]") ?>
              </div>
              <?php the_content() ?>
              <?php endwhile; */ ?>
        </div>
    </div>
</div>